from django.conf.urls import patterns, include, url
from anuwadakApp.views import *
from anuwadakApp.view1 import *

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^words/?$',scrape_data),
    url(r'^language/?$',load_language),
    url(r'^$',home_page),
    url(r'^article/?$',load_article),
    url(r'^compare/?$',compare_page),
    
    
    # url(r'^$', 'anuwadak.views.home', name='home'),
    # url(r'^anuwadak/', include('anuwadak.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
