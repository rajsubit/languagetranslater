from django.db import models


# Create your models here.
class Language(models.Model):
    """Provides information about the Language"""
    language_name = models.CharField(max_length=100, blank=True)
    # def __init__(self, arg):
    #     super(Language, self).__init__()
    #     self.arg = arg


class  Article(models.Model):
    """Provides information of article"""
    # def __init__(self, arg):
    #     super(Article, self).__init__()
    #     self.arg = arg

    article_name = models.CharField(max_length=200, blank=True)
    author_name = models.CharField(max_length=200, blank=True)
    language = models.ForeignKey(Language, blank=True)


class Sentence(models.Model):
    """docstring for Sentence"""
    # def __init__(self, arg):
    #     super(Sentence, self).__init__()
    #     self.arg = arg

    sentence_text = models.TextField(max_length=1000, blank=True)
    article = models.ForeignKey(Article, blank=True)
    language = models.ForeignKey(Language, blank=True)
    sequence_num = models.IntegerField(null=True)
    translated = models.NullBooleanField()
