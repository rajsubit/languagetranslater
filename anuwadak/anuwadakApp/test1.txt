India vs South Africa
4th ODI, Vadodra
17 March 2000
Over 45
`Why the fuck did you have to move?' Ishaan's scream drowned out the
stadium din on the TV. I had shifted up to a sofa from the floor.
`Huh?' I said. We were in Ishaan's house — Ishaan, Omi and I. Ishaan's mom
had brought in tea and khakra for us. 'It is more comfortable to snack on the
sofa. That is why I moved.'
`Tendulkar's gone. Fuck, now at this stage. Omi, don't you dare move now.
Nobody moves for the next five overs.'
I looked at the TV. We were chasing 283 to win. India's score a ball ago was
256-2 after forty-five overs. Twenty-seven runs in five overs, with eight wickets to
spare and Tendulkar on the crease. A cakewalk. The odds were still in India's
favour, but Tendulkar was out. And that explained the frowns on Ishaan's
forehead.
'The khakra's crispy,' Omi said. Ishaan glared at Omi, chiding him for his
shallow sensory pleasure in a moment of national grief. Omi and I kept our tea
cups aside and looked suitably mournful.
The crowd clapped as Tendulkar made his exit. Jadeja came to the crease and
added six more runs. End of forty-six overs, India 262/3. Twenty-one more runs
to win in four overs, with seven wickets in hand.

TRUE!—nervous—very, very dreadfully nervous I had been 
and  am;  but why will you say  that I  am mad? The  disease 
had sharpened my senses—not destroyed—not dulled them. 
Above all was the sense of hearing acute. I heard all things in 
the  heaven  and  in  the  earth.  I  heard  many  things  in  hell. 
How,  then,  am  I  mad?  Hearken!  and observe  how 
healthily—how calmly I can tell you the whole story. 
It is  impossible  to say  how  first the  idea  entered  my 
brain;  but  once conceived,  it  haunted  me  day  and night. 
Object there was none. Passion there was none. I loved the 
old man. He had never wronged me. He had never given me 
insult. For  his  gold I  had no desire. I think it was  his  eye! 
yes,  it  was  this!  One  of  his  eyes  resembled  that  of  a 
vulture—a pale blue eye, with a film over it. Whenever it fell 
upon  me,  my  blood  ran  cold;  and  so  by  degrees—very 
gradually—I  made  up  my  mind  to  take  the  life  of  the  old 
man, and thus rid myself of the eye for ever.