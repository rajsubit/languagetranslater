# Create your views here.
from django.conf import settings
from anuwadakApp.models import *
import os
import re


def load_language(request):
    lang = ['English', 'Nepali']
    for item in lang:
        language = Language()
        language.language_name = item
        language.save()

def load_article(request):
    language = Language.objects.get(language_name = 'English')
    article = Article()
    article.article_name = 'Romeo and JulietRomeo and Juliet'
    article.author_name = 'William Shakespeare'
    article.language = language
    article.save()
   

def scrape_data(request):
    lang = ['English', 'Nepali']
    for item in lang:
        language = Language()
        language.language_name = item
        language.save()
    language = Language.objects.get(language_name = 'English')
    article = Article()
    article.article_name = 'Romeo and Juliet'
    article.author_name = 'William Shakespeare'
    article.language = language
    article.save()

    new_file = (open(settings.APP_ROOT+'/anuwadakApp/'+'test.txt', 'rb'))
    data = new_file.read().replace('\n',' ')    #print type(data)
    #sent = re.split(r'[.?]+',data)
    sent = re.split(r'[.]+',data)
    for items in sent:
        language = Language.objects.get(language_name = 'English')
        article = Article.objects.get(article_name = 'Romeo and Juliet')
        sentence = Sentence()
        sentence.sentence_text = items + '.'
        sentence.article = article
        sentence.language = language
        sentence.sequence_num = sent.index(items)
        sentence.translated = False
        sentence.save()
        
        
   