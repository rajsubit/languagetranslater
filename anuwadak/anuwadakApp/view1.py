# Create your views here.
from django.conf import settings
from anuwadakApp.models import *
from django.views.decorators.csrf import csrf_exempt
import os
import re
from django.shortcuts import render_to_response
import operator
@csrf_exempt
def home_page(request):
    if request.method == 'POST':
        print "success"
        translatedval = request.POST['displaytextbox']
        article_id =    request.POST['article']
        sequence_id =    request.POST['sequence']
        article = Article.objects.get(id = article_id)
        language = Language.objects.get(language_name = 'Nepali')
        article = Article.objects.get(id = article_id)
        sentence = Sentence()
        sentence.sentence_text = translatedval
        sentence.article = article
        sentence.language = language
        sentence.sequence_num = sequence_id
        sentence.translated = True
        sentence.save()


        language = Language.objects.get(language_name = 'English')
        sentence = Sentence.objects.get(language=language, article=article, sequence_num=sequence_id)
        sentence.translated = True
        sentence.save()


    article_id = 1
    article = Article.objects.get(id = article_id)



    language = Language.objects.get(language_name = 'English')

    sentence = Sentence.objects.filter(language=language, translated = False)

    parameter = {}
    if(len(sentence) == 0):
        parameter['sentence'] = {'sentence_text':'Conversion Completed', 'article':'', 'sequence_num':''}
    parameter['sentence'] = sentence[0]
    parameter['article'] = article
    return render_to_response('main.html', parameter)

@csrf_exempt
def compare_page(request):
    article_id = 1
    article = Article.objects.get(id = article_id)
    language = Language.objects.get(language_name = 'English')
    sentences = Sentence.objects.filter(language=language, article=article)
    sent_list =list(sentences)
    sent_list.sort(key = operator.attrgetter('sequence_num'))
    document = ''
    for item in sent_list:
        document += '<span id="original_'+str(item.sequence_num)+'">'+item.sentence_text + '</span> '
    eng_doc = document

    language = Language.objects.get(language_name = 'Nepali')
    sentences = Sentence.objects.filter(language=language, article=article)
    sent_list =list(sentences)
    sent_list.sort(key = operator.attrgetter('sequence_num'))
    document = ''
    for item in sent_list:
        document += '<span id="duplicate_'+str(item.sequence_num)+'" onMouseOut="unhighlightoriginal('+str(item.sequence_num)+')" onMouseOver="highlightoriginal('+str(item.sequence_num)+')">'+item.sentence_text + '</span> '
    nep_doc = document

    article = Article.objects.get(id = article_id)

    parameter={}
    parameter['english'] = eng_doc
    parameter['nepali'] = nep_doc
    parameter['article'] = article
    return render_to_response('compare.html', parameter)


